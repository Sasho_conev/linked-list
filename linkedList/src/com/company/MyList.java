package com.company;

public class MyList {
    private Node head;
    private Node tail;
    private int size;

    public MyList(int value){
        head=new Node(value);
        tail=head;
        size=1;
    }

    public MyList(){
        head=null;
        tail=null;
        size=0;
    }

    public boolean isEmpty(){
        if(head==null)return true;
        return false;
    }


    public void add(int value){
        if(isEmpty()){
            head=new Node(value);
            tail=head;
        }
        else{
            tail.setNext(new Node(value));
            tail=tail.getNext();
        }
        size++;
    }

    public void insertFirst(int value){
        Node newNode=new Node(value);
        newNode.setNext(head);
        head=newNode;
    }

    public void insert(int value,int index){

        if(index==size){
            add(value);
            return;
        }else if(index==0){
            insertFirst(value);
            return;
        }else if(index<0){
            System.out.println("Invalid index!");
            return;
        }


        if(index>size){
            System.out.println("Invalid index");
            return;
        }
        else{
            Node newNode=new Node(value);
            int current=0;
            Node currentNode=head;
            while(current!=index-1){
                currentNode=currentNode.getNext();
                current++;
            }
            newNode.setNext(currentNode.getNext());
            currentNode.setNext(newNode);
        }
        size++;
    }

    public void printList(){
        Node current=head;
        while(current!=null){
            System.out.println(current.getValue());
            current=current.getNext();
        }
    }


    public int getSize(){
        return size;
    }

    public int getM(int m){
        if(m==0)return tail.getValue();

        int index=size-1-m;
        if(index<0){
            System.out.println("Invalid m");
            return -1;
        }

        Node current=head;

        for(int i=0;i<index;i++){
            current=current.getNext();
        }

        return current.getValue();
    }


    public void removeFirst(){
        head=head.getNext();
        size--;
    }



    public void remove(int index){
        if(index<0 || index>=size){
            System.out.println("Invalid index");
            return;
        }

        if(index==0){
            removeFirst();
            return;
        }



        int current=0;
        Node currentNode=head;
        while(current!=index-1){
            currentNode=currentNode.getNext();
            current++;
        }
        currentNode.setNext(currentNode.getNext().getNext());
        if (currentNode.getNext()==null)tail=currentNode;

    }





}
